FROM openjdk:8-jre-slim

WORKDIR /home

ADD ./build/libs/cup-supply-0.4.jar app.jar

CMD java -jar ${ADDITIONAL_OPTS} app.jar

EXPOSE 9001
